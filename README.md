# How to

Based on IoTops repo at https://iotops.gitlab.io/charts

To add our repo to your helm, just type:

    helm repo add jinetes https://jsl3.gitlab.io/helmrepo

To add a new chart, add it like a git submodule.

## Charts available

### PeerTube

[Peertube](https://github.com/Chocobozzz/PeerTube) is an ActivityPub-federated video streaming platform using P2P directly in your web browser https://joinpeertube.org/

```bash
helm install jinetes/peertube
```

For customization see the [chart at our Repository](https://gitlab.com/jsl3/helmcharts/-/tree/master/peertube). 

![](https://gitlab.com/jsl3/charla-k8s-la-punta-del-iceberg/-/raw/master/presentation/slides/images/jinetes-01.png)
